import secrets
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank.models import Account, Customer


class Command(BaseCommand):
    def handle(self, **options):
        # Create demo data if the database is empty
        if User.objects.count() == 0 or Account.objects.count() == 0 or Customer.objects.count() == 0:
            print('Creating superuser')
            admin = User.objects.create_superuser('tacobank_admin', email='admin@tacobank.dk', password='tacobank0')
            admin.is_active = True
            admin.is_admin = True
            admin.save()

            # Add demo data
            print('Adding demo data ...')
            bank_user = User.objects.create_user('bank', email='', password=secrets.token_urlsafe(64))
            bank_user.is_active = False
            bank_user.save()
            # User 1
            rachel_user = User.objects.create_user('rachelhvo', email='rachelvo@gmail.com', password='123456')
            rachel_user.first_name = 'Rachel'
            rachel_user.last_name = 'Vo'
            rachel_user.save()

            # User 2
            tiffany_user = User.objects.create_user('tiffanytran', email='tiffanytran@gmail.com', password='123456')
            tiffany_user.first_name = 'Tiffany'
            tiffany_user.last_name = 'Tran'
            tiffany_user.save()

            # Customer 1
            rachel_customer = Customer(user=rachel_user, personal_id='1234991234', phone='43126758')
            rachel_customer.save()
            rachel_account = Account.objects.create(user=rachel_user, name='Nemkonto')
            rachel_account.save()

            # Customer 2
            tiffany_customer = Customer.objects.create(user=tiffany_user, personal_id='1212991845', phone='87256409')
            tiffany_customer.save()
            tiffany_account = Account.objects.create(user=tiffany_user, name='Nemkonto')
            tiffany_account.save()

        else:
            print('Demodata is already added')
