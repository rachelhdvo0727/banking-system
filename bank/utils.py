from django.core.validators import RegexValidator


bank_list = [{'reg_number': 1111, 'bank_name': 'Nordea', 'port': '8000'},
             {'reg_number': 1212,
              'bank_name': 'Danske Bank', 'port': '8080'},
             {'reg_number': 1313, 'bank_name': 'Jyske Bank', 'port': '7000'}]


def personal_id():
    alphanumeric = RegexValidator(
        r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
    return alphanumeric


def search_reg_number(reg_number):
    for bank in bank_list:
        if reg_number == bank['reg_number']:
            return bank['port']
