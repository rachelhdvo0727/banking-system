from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.validators import RegexValidator

from .models import Customer, Account


class TransferForm(forms.Form):
    amount = forms.DecimalField(label='Amount', max_digits=10)
    debit_account = forms.ModelChoiceField(
        label='Debit Account', queryset=Customer.objects.none())
    debit_text = forms.CharField(label='Debit Account Text', max_length=25)
    credit_account = forms.IntegerField(label='Credit Account Number')
    credit_text = forms.CharField(label='Credit Account Text', max_length=25)

    def clean(self):
        super().clean()

        # Ensure credit account exist
        credit_account = self.cleaned_data.get('credit_account')
        try:
            Account.objects.get(pk=credit_account)
        except ObjectDoesNotExist:
            self._errors['credit_account'] = self.error_class(
                ['Credit account does not exist.'])

        # Ensure positive amount
        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(
                ['Amount must be positive.'])

        return self.cleaned_data


class ExternalTransferForm(forms.Form):
    amount = forms.DecimalField(label='Amount', max_digits=10)
    # To
    credit_account_number = forms.IntegerField(label='Credit Account Number')
    receiver_username = forms.CharField(label="Receiver's username")
    bank_registered_number = forms.IntegerField(
        label="Bank's registered number", validators=[RegexValidator(r'\d{4,4}', 'Number must be 4 digits', 'Invalid number')])
    credit_account = f'{credit_account_number} :: {receiver_username} :: {bank_registered_number}'
    credit_text = forms.CharField(label='Note to receiver', max_length=25)
    # From
    debit_account = forms.ModelChoiceField(
        label='Debit Account', queryset=Customer.objects.none())
    debit_text = forms.CharField(
        label='Note', max_length=25)

    def clean(self):
        super().clean()

        # Amount cannot be negative
        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(
                ['Amount must be positive.'])

        return self.cleaned_data


class NewUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def clean(self):
        super().clean()
        username = self.cleaned_data.get('username')
        if User.objects.filter(username=username):
            self._errors['username'] = self.error_class(
                ['Username already exists.'])
        return self.cleaned_data


class UserForm(forms.ModelForm):
    username = forms.CharField(label='Username', disabled=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ('rank', 'personal_id', 'phone')


class NewAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ('name',)
