from rest_framework import serializers
from .models import Transaction, Ledger, Account


class TransactionsSerializer(serializers.ModelSerializer):
    class Meta():
        model = Transaction
        fields = '__all__'

    def create(self, validated_data):
        try:
            account_to_deposit = Account.objects.get(id=int(validated_data['credit_account'][0]))
            Ledger.deposit(amount=validated_data['amount'], debit_account=validated_data['debit_account'], debit_text=validated_data['text'], credit_account=account_to_deposit, credit_text=validated_data['text'])
        except Account.DoesNotExist:
            return Transaction.objects.create(**validated_data)

        return Transaction.objects.create(**validated_data)
