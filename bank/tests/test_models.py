import unittest
from bank.models import Customer
from django.contrib.auth.models import User
from bank.models import Customer


class UserTest(unittest.TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(first_name='Tiffany', last_name='Tran')

    def test_username_label(self):
        user = User.objects.get(id=4)
        field_label = user._meta.get_field('username').verbose_name
        self.assertEqual(field_label, 'username')
        print(user.username)

    def test_full_name(self):
        user = User.objects.get(id=4)
        expected_full_name = f'{user.first_name} {user.last_name}'
        self.assertNotEqual(str(user), expected_full_name)
        print(expected_full_name)

    def test_length(self):
        customer = Customer.objects.get(personal_id=1212991845)
        personal_id = customer.personal_id
        max_length = personal_id.max_length
        self.assertEqual(max_length, 10)
        print(personal_id)
