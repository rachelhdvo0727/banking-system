from django.test import TestCase
from django.urls import reverse

from django.contrib.auth.models import User


class DashboardViewTest(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(
            username='Tiff', password='test123456')
        test_user.save()

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('/'))
        self.assertRedirects(response, '/account/login')

    def test_logged_in_correctly(self):
        # login = self.client.login(username='Tiff', password='test123456')
        response = self.client.get(reverse('/'))

        # Check if the user is logged in
        self.assertEqual(str(response.context['user']), 'Tiff')
        # Check that the response was a success
        self.assertEqual(response.status_code, 200)
