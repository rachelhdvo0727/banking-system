from django.urls import path
from rest_framework.routers import SimpleRouter
from .api import TransactionView
from .views import ExternalTransfer
from . import views

app_name = "bank"

router = SimpleRouter()
# router.register('transactions', views.TransactionView, basename='transactions')


urlpatterns = [
    path('', views.index, name='index'),

    path('dashboard/', views.dashboard, name='dashboard'),
    path('account_details/<int:pk>/',
         views.account_details, name='account_details'),
    path('transaction_details/<int:transaction>/',
         views.transaction_details, name='transaction_details'),

    path('make_transfer/', views.make_transfer, name='make_transfer'),
    path('make_loan/', views.make_loan, name='make_loan'),
    path('external_transfer/', ExternalTransfer.as_view(),
         name='external_transfer'),
    path('api/v1/transactions/',
         TransactionView.as_view(), name='transactions'),

    path('staff_dashboard/', views.staff_dashboard, name='staff_dashboard'),
    path('staff_search_partial/', views.staff_search_partial,
         name='staff_search_partial'),
    path('staff_customer_details/<int:pk>/',
         views.staff_customer_details, name='staff_customer_details'),
    path('staff_account_list_partial/<int:pk>/',
         views.staff_account_list_partial, name='staff_account_list_partial'),
    path('staff_account_details/<int:pk>/',
         views.staff_account_details, name='staff_account_details'),
    path('staff_new_account_partial/<int:user>/',
         views.staff_new_account_partial, name='staff_new_account_partial'),
    path('staff_new_customer/', views.staff_new_customer,
         name='staff_new_customer'),
    path('account_details/<int:pk>/bank_report/',
         views.create_pdf_report, name='bank_report')
]
