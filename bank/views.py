from decimal import Decimal
from secrets import token_urlsafe
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, reverse, get_object_or_404
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import status
import requests

from .forms import TransferForm, ExternalTransferForm, UserForm, CustomerForm, NewUserForm, NewAccountForm
from .models import Account, Ledger, Customer, Transaction
from .serializers import TransactionsSerializer
from .errors import InsufficientFunds
from .utils import search_reg_number

from xhtml2pdf import pisa
from django.template.loader import get_template


def index(request):
    if request.user.is_staff:
        return HttpResponseRedirect(reverse('bank:staff_dashboard'))
    else:
        return HttpResponseRedirect(reverse('bank:dashboard'))


# Customer views

def dashboard(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    accounts = request.user.customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'bank/dashboard.html', context)


def account_details(request, pk):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    account = get_object_or_404(Account, user=request.user, pk=pk)
    context = {
        'account': account,
    }
    return render(request, 'bank/account_details.html', context)


def transaction_details(request, transaction):
    movements = Ledger.objects.filter(transaction=transaction)
    if not request.user.is_staff:
        if not movements.filter(account__in=request.user.customer.accounts):
            raise PermissionDenied('Customer is not part of the transaction.')

    context = {
        'movements': movements,
    }
    return render(request, 'bank/transaction_details.html', context)


def make_transfer(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if request.method == 'POST':
        form = TransferForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customer.accounts
        if form.is_valid():
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(
                pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            credit_account = Account.objects.get(
                pk=form.cleaned_data['credit_account'])
            credit_text = form.cleaned_data['credit_text']
            try:
                transfer = Ledger.transfer(
                    amount, debit_account, debit_text, credit_account, credit_text)
                return transaction_details(request, transfer)
            except InsufficientFunds:
                context = {
                    'title': 'Transfer Error',
                    'error': 'Insufficient funds for transfer.'
                }
                return render(request, 'bank/error.html', context)
    else:
        form = TransferForm()
    form.fields['debit_account'].queryset = request.user.customer.accounts
    context = {
        'form': form,
    }
    return render(request, 'bank/make_transfer.html', context)


class ExternalTransfer(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "bank/external_transfer.html"

    def get(self, request, format=None):
        assert not request.user.is_staff
        form = ExternalTransferForm()
        form.fields['debit_account'].queryset = request.user.customer.accounts
        return Response({'form': form}, status=status.HTTP_200_OK)

    def post(self, request):
        form = ExternalTransferForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customer.accounts

        if form.is_valid():
            # Add the necessary fields for external transfer
            amount = form.cleaned_data['amount']
            # To
            credit_account_number = form.cleaned_data['credit_account_number']
            receiver_username = form.cleaned_data['receiver_username']
            bank_registered_number = form.cleaned_data['bank_registered_number']
            credit_account = f'{credit_account_number} :: {receiver_username} :: {bank_registered_number}'
            credit_text = form.cleaned_data['credit_text']
            # From
            debit_account = Account.objects.get(
                pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            try:
                # Register this transaction in Ledger table
                transfer = Ledger.external_transfer(
                    amount, debit_account, debit_text, credit_account, credit_text)
                # Register this transaction to Transaction table
                transaction_data = request.data
                transaction_serializer = TransactionsSerializer(
                    data=transaction_data)
                if not transaction_serializer.is_valid():
                    return Response(transaction_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

                transaction_serializer.save(
                    transaction=transfer.id, debit_account=debit_account, credit_account=credit_account, text=debit_text, )

                # Also send this to another bank, according to instance's port number
                data = {
                    "amount": amount,
                    "transaction": transfer.id,
                    "debit_account":  debit_account,
                    "credit_account": credit_account,
                    "text": debit_text,
                }

                port_to_send = search_reg_number(bank_registered_number)
                url = f'http://127.0.0.1:{port_to_send}/api/v1/transactions/'
                r = requests.post(url, data=data)
                r.raise_for_status()
                # print('sent to another bank', r.status_code)

                return transaction_details(request, transfer)
            except InsufficientFunds:
                context = {'title': 'Transfer Error',
                           'error': 'Insufficient funds for transfer.'}
                return render(request, 'bank/error.html', context)

        return Response({'form': form}, status=status.HTTP_200_OK)


def make_loan(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if request.user.customer.can_make_loan == 'B':
        context = {'title': 'Create Loan Error',
                   'error': 'Loan could not be completed. Loan can not be offered to Basic rank.'}
        return render(request, 'bank/error.html', context)

    if request.method == 'POST':
        request.user.customer.make_loan(Decimal(
            request.POST['amount']), request.POST['name'], request.POST['payment_period'])
        return HttpResponseRedirect(reverse('bank:dashboard'))
    return render(request, 'bank/make_loan.html', {})


# Staff views
def staff_dashboard(request):
    assert request.user.is_staff, 'Customer user routing staff view.'
    transactions = Transaction.objects.all()

    context = {
        'transactions': transactions,
    }

    return render(request, 'bank/staff_dashboard.html', context)


def staff_search_partial(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    search_term = request.POST['search_term']
    customers = Customer.search(search_term)
    context = {
        'customers': customers,
    }
    return render(request, 'bank/staff_search_partial.html', context)


def staff_customer_details(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk)
    if request.method == 'GET':
        user_form = UserForm(instance=customer.user)
        customer_form = CustomerForm(instance=customer)
    elif request.method == 'POST':
        user_form = UserForm(request.POST, instance=customer.user)
        customer_form = CustomerForm(request.POST, instance=customer)
        if user_form.is_valid() and customer_form.is_valid():
            user_form.save()
            customer_form.save()
    new_account_form = NewAccountForm()
    context = {
        'customer': customer,
        'user_form': user_form,
        'customer_form': customer_form,
        'new_account_form': new_account_form,
    }
    return render(request, 'bank/staff_customer_details.html', context)


def staff_account_list_partial(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk)
    accounts = customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'bank/staff_account_list_partial.html', context)


def staff_account_details(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    account = get_object_or_404(Account, pk=pk)
    context = {
        'account': account,
    }
    return render(request, 'bank/account_details.html', context)


def staff_new_account_partial(request, user):
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST':
        new_account_form = NewAccountForm(request.POST)
        current_user = User.objects.get(pk=user)
        all_accounts = Account.objects.filter(user=user)

        if new_account_form.is_valid():
            if all_accounts.count() == 0:
                Account.objects.create(user=current_user, name="Nemkonto")
            else:
                Account.objects.create(
                    user=current_user, name=new_account_form.cleaned_data['name'])
    return HttpResponseRedirect(reverse('bank:staff_customer_details', args=(user,)))


def staff_new_customer(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST':
        new_user_form = NewUserForm(request.POST)
        customer_form = CustomerForm(request.POST)
        if new_user_form.is_valid() and customer_form.is_valid():
            username = new_user_form.cleaned_data['username']
            first_name = new_user_form.cleaned_data['first_name']
            last_name = new_user_form.cleaned_data['last_name']
            email = new_user_form.cleaned_data['email']
            password = token_urlsafe(16)
            rank = customer_form.cleaned_data['rank']
            personal_id = customer_form.cleaned_data['personal_id']
            phone = customer_form.cleaned_data['phone']
            try:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                    email=email,
                    first_name=first_name,
                    last_name=last_name
                )
                print(
                    f'********** Username: {username} -- Password: {password}')
                Customer.objects.create(
                    user=user, rank=rank, personal_id=personal_id, phone=phone)
                return staff_customer_details(request, user.pk)
            except IntegrityError:
                context = {
                    'title': 'Database Error',
                    'error': 'User could not be created.'
                }
                return render(request, 'bank/error.html', context)
    else:
        new_user_form = NewUserForm()
        customer_form = CustomerForm()
    context = {
        'new_user_form': new_user_form,
        'customer_form': customer_form,
    }
    return render(request, 'bank/staff_new_customer.html', context)


def create_pdf_report(request, pk):
    account = get_object_or_404(Account, user=request.user, pk=pk)
    template_path = 'bank/bank_statement.html'
    context = {
        'account': account,
    }
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename="bank_report.pdf"'
    template = get_template(template_path)
    html = template.render(context)

    pisa_status = pisa.CreatePDF(
        html, dest=response)
    if pisa_status.err:
        return HttpResponse('Error' + html)
    return response
