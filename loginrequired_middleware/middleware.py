from django.conf import settings
from django.urls import resolve, reverse
from django.http import HttpResponseRedirect


class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        assert hasattr(request, 'user')
        if not request.user.is_authenticated:
            current_route_name = resolve(request.path_info).url_name
            # print(current_route_name)
            if not current_route_name in settings.AUTH_EXEMPT_ROUTES:
                return HttpResponseRedirect(reverse(settings.AUTH_LOGIN_ROUTE))
        # print(request.path_info)
        response = self.get_response(request)

        return response
