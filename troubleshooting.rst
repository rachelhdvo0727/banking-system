Troubleshooting
===============

This page will guide developers on how to troubleshoot errors and problems that might occur during development.

Linting
_______

If the linting process fails, look at the terminal to see the errors that have been filtered out by Pylama. Pylama should also include error codes and solution to fix them. Simply follow these instructions to remove the errors.

If the error code is provided without a solution, do an online research to find what the error is and what could be done with fix it.

If a solution can not be found, developers may put the error code into the ignore file.

Git
___

Here is a list of useful git commands to use in case of conflicts and problems regarding Git.

.. list-table::
   :widths: 40 40
   :header-rows: 1

   * - Git commands
     - What it does
   * - git init
     - Initialise a local Git repository
   * - git clone repo_url
     - Clone public repository
   * - git clone ssh://git@github.com/[username]/[repository-name].git
     - Clone private repository
   * - git status
     - Check status
   * - git add .
     - Add all files to the staging area
   * - git branch [branch name]
     - Create a new branch
   * - git branch
     - List of branches (the asterisk denotes the current branch)
   * - git commit -m "[commit message]"
     - Commit changes
   * - git rm -r [filename.txt]
     - Remove a file (or a folder)
   * - git branch -d [branch name]
     - Delete a branch
   * - git push origin --delete [branch name]
     - Delete a remote branch
   * - git checkout -b [branch name]
     - Create a new branch and switch to it
   * - git checkout [branch name]
     - Switch to a branch
   * - git merge
     - Merge a branch into the active branch
   * - git merge [source branch] [target branch]
     - Merge a branch into a target branch
   * - git push origin [branch name]
     - Push a branch to your remote repository
   * - git push
     - Push changes to remote repository
   * - git pull origin [branch name]
     - "Pull changes from remote repository
   * - git log
     - View changes
   * - git revert commitid
     - Revert commit changes
