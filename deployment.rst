Deployment
==========

Our Taco Bank is not made ready for production therefore there is no guideline for this deployment process.

Deploy documentation to Gitlab pages
____________________________________

However, the project has a continuous deployment that publishes our Sphinx documentation onto Gitlab pages:

To make edits for existing pages:

#. Check out onto the feature/documentation branch
#. Make edits in the appropriate restructured text file that you wish to change
#. Git add, commit and push the changes onto the current branch
#. Merge the commit to the main branch
#. The CI/CD pipeline will process the commit and deploy the pages onto Gitlab
#. You can see the changes on: `https://rachelhdvo0727.gitlab.io/banking-system/ <https://rachelhdvo0727.gitlab.io/banking-system/>`_

To create a new page:

#. Check out onto the feature/documentation branch
#. Create a new file with a .rst extension
#. Add content into the new file and save it
#. Go into index.rst and add the name of the newly created file under Contents list
#. Git add, commit and push the changes onto the current branch
#. Merge the commit to the main branch
#. The CI/CD pipeline will process the commit and deploy the pages onto Gitlab
#. You can see the changes on: `https://rachelhdvo0727.gitlab.io/banking-system/ <https://rachelhdvo0727.gitlab.io/banking-system/>`_

