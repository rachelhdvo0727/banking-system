FROM python:3.9-slim-buster

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt-get update && \
    apt-get install -y libpq-dev python3-dev python-dev python-psycopg2 python3-psycopg2 gcc

ADD requirements.txt /
RUN pip install --upgrade pip
RUN pip install djangorestframework
RUN pip install django-rest-auth
RUN pip install Django==3.2.7
RUN pip install django-formtools
RUN pip install phonenumbers
RUN pip install django-two-factor-auth
RUN pip install django-otp-yubikey
RUN pip install requests
RUN pip install xhtml2pdf
RUN pip install -U sphinx
RUN pip install sphinx_rtd_theme
RUN pip install django-docs
RUN pip install psycopg2==2.8.6 --ignore-installed
RUN pip install -r requirements.txt

WORKDIR /app
COPY . /app

COPY ./entrypoint.sh /
ENTRYPOINT ["sh", "/entrypoint.sh"]

